from ddv.printing import (
    print_h1,
    print_h2,
    print_h3,
    print_ok,
    print_pass,
    print_fail,
    print_warn,
    print_error,
    print_fatal,
)


print_h1("Header One")
print("Lorem ipsum")
print_h2("Header Two")
print("Lorem ipsum")
print_h3("Header Three")
print("Lorem ipsum")

print()

print_ok("OK")
print_pass("Test passed.\nThis test has passed")
print_fail("Test failed.\nThis test has failed")
print_warn("Warning.\nThese are warning details")
print_error("Error.\nThis is error information")
print_fatal("Fatal.\nThis is more information")
