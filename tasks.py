from invoke.context import Context

# PYTEST_COMMAND_ARGS = "--cov=ddv.printing --cov-report term-missing --cov-branch tests"
SRC_DIR = "src"
EXAMPLES_DIR = "examples"
CONTEXT = Context()


def mypy():
    with CONTEXT.cd(SRC_DIR):
        CONTEXT.run("poetry run python -m mypy ddv/printing", pty=True, warn=True)


def flake8():
    with CONTEXT.cd(SRC_DIR):
        CONTEXT.run("poetry run python -m flake8 ddv/printing", pty=True, warn=True)


def check():
    mypy()
    flake8()


def examples():
    with CONTEXT.cd(EXAMPLES_DIR):
        CONTEXT.run(
            "poetry run python example.py",
            env={"PYTHONPATH": f"../{SRC_DIR}"},
            pty=True,
            warn=True,
        )



# def test():
#     with CONTEXT.cd(SRC_DIR):
#         CONTEXT.run(
#             f"poetry run pytest {PYTEST_COMMAND_ARGS}", pty=True, warn=True,
#         )


# def watch():
#     with CONTEXT.cd(SRC_DIR):
#         CONTEXT.run(
#             f"ptw . -- {PYTEST_COMMAND_ARGS}", env={"PYTHONPATH": "."}, pty=True,
#         )


# def docs():
#     with CONTEXT.cd(SRC_DIR):
#         CONTEXT.run(
#             f"poetry run pydoc-markdown --render-toc -p ddv.printing > ../docs/reference.md ",
#             pty=True,
#         )
