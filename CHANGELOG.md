# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2020-06-27

### Added
- Add methods for printing headers (1, 2, and 3)
- Add methods for printing ok, pass, fail, warn, error, and fail messages
